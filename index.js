#!/usr/bin/env node
var program = require('commander');
var fs = require('fs');
var path = require('path');
var logger = require('./tool/logger');
var rootFolder = __dirname;
var configFile = path.join(rootFolder, './settings.json');
var defaultThemesFolder = 'sources';
var outputFolder;
var sourceFolder;

program.version('0.0.2')
    .usage('[options]')
    .option('-c, --config [configFile]', 'A config file in another location.', function (val) {
        configFile = val;
    })
    .option('-o, --output [outputFolder]', 'A different output folder.', function (val) {
        outputFolder = val;
    })
    .option('-s, --source [sourceFolder]', 'A different source folder.', function (val) {
        sourceFolder = val;
    })
    .parse(process.argv);

var config = {};

try {
    var settings = fs.readFileSync(configFile, 'utf8');
    config = JSON.parse(settings);
}
catch (ex) {
    logger.error('The settings file "' + configFile + '" was not found.');
    process.exit();
}

if (outputFolder) {
    var p = path.parse(outputFolder);
    if (p.ext) {
        logger.error('Invalid output directory: "' + outputFolder + '"');
        process.exit();
    }
    config.outputFolder = outputFolder;
}

if (sourceFolder) {
    var p = path.parse(sourceFolder);
    if (p.ext) {
        logger.error('Invalid source directory: "' + sourceFolder + '"');
        process.exit();
    }
    config.sourceFolder = sourceFolder;
}

try {
    var stat = fs.statSync(config.sourceFolder || defaultThemesFolder);
    if (!stat.isDirectory())
        throw new Error();
} catch (ex) {
    logger.error('Could not find the source folder.');
    process.exit();
}

try {
    var stat = fs.statSync(path.join((config.sourceFolder || defaultThemesFolder), 'system'));
    if (!stat.isDirectory())
        throw new Error();
} catch (ex) {
    logger.error('You do not have a "system" theme.');
    process.exit();
}

var themeBuilder = require('./tool/themeBuilder')(config);
themeBuilder.begin();