var colors = require('colors');

var logger = module.exports = {};

logger.info = function (text) {
    console.log(text.cyan);  
};

logger.keyValue = function (key, value) {
    console.log((key + ':').green + ' ' + value.white);
};

logger.error = function (text) {
    console.log(text.red);
};