var configKeys = require('./configKeys');

var utility = module.exports = {};

utility.generateRandomName = function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

utility.mapImages = function (obj, fn) {
    var result = {}; 
    for (var key in configKeys) {
        result[key] = fn(obj[key]);
    }
    return result;
};

//Validates theme input and generates object with final property names.
utility.validateThemeParameters = function (themeObject, identification) {
    var obj = {};
    
    for (var expected in configKeys.keys) {
        var key = configKeys.keys[expected];
        var value = themeObject[expected];
        
        if (!value) {
            //if there's a property missing, execution fails.
            logger.error('Property "' + expected + '" not found on theme "' + identification + '".');
            process.exit();
        }
        
        obj[key] = value;
    }
    
    return obj;
};

//Sets Less Variables
utility.setLessVariables = function (less, data) {
    var finalLess;
    
    for (var key in data) {
        finalLess = (finalLess || less).replace('{{' + key + '}}', data[key]);
    }
    
    return finalLess;
};