var defaultThemesFolder = 'sources';
var settings;

var logger = require('./logger');
var fs = require('fs');
var glob = require('glob');
var path = require('path');
var mkdirp = require('mkdirp');
var rimraf = require('rimraf');
var utility = require('./utility');

var processTheme = require('./processTheme');
var index = 0;
var themesObject = {};
var files = [];
var cssFileNames = {};

function finishProcessing() {
    logger.info('Writing final CSS and JSON...');
    var finalCss = '@import "system/' + cssFileNames.system + '";\n';
    for (var key in themesObject) {
        if (key !== 'system')
            finalCss += '@import "' + key + "/" + cssFileNames[key] + '";\n';
    }

    fs.writeFileSync(path.join(settings.outputFolder, 'themes.css'), finalCss);
    fs.writeFileSync(path.join(settings.outputFolder, 'themes.json'), JSON.stringify(themesObject, null, 4));
    logger.info('Done.');
}

function processNextTheme() {
    var file = files[index++];
    var cssFileName = 'theme_' + utility.generateRandomName() + '.css';
    processTheme(file, cssFileName, settings)
    .then(function (result) {
        themesObject[result.identification] = result.data;
        cssFileNames[result.identification] = cssFileName;
        if (index < files.length)
            processNextTheme();
        else
            finishProcessing();
    })
    .catch(function (err) {
        logger.error('There was an error processing your themes.');
        console.log(err);
    });
}

module.exports = function (config) {
    settings = config;
    return {
        begin: function () {
            logger.info('Looking for themes...');
            files = glob.sync((settings.sourceFolder || defaultThemesFolder) + '/**/theme.properties');
            
            if (!files || files.length === 0) {
                logger.error('No theme "theme.properties" files found inside the source folder.');
                process.exit();
            }
            
            for (var i in files) {
                logger.keyValue('Found theme', files[i]);    
            }

            logger.info('Generating themes...');
            
            rimraf.sync(settings.outputFolder);
            
            processNextTheme();
        }  
    };
};



