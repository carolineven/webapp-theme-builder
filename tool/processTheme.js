var tempFolder = 'themes_temp';
var rootFolder = __dirname;

var configKeys = require('./configKeys');
var logger = require('./logger');
var propertiesParser = require('properties-parser');
var less = require('less');
var rimraf = require('rimraf');
var path = require('path');
var fs = require('fs');
var mkdirp = require('mkdirp');
var Promise = require('promise');
var utility = require('./utility');

var themeLess = fs.readFileSync(path.join(rootFolder, '/templates/theme.less'), 'utf8');
var themeWrapperLess = fs.readFileSync(path.join(rootFolder, '/templates/theme_wrapper.less'), 'utf8');
var variablesLess =  fs.readFileSync(path.join(rootFolder, '/templates/variables.less'), 'utf8');

function generateJson(propertyFile, identification) {
    return new Promise(function (fulfill, reject) {
       fs.readFile(propertyFile, function (err, data) {
            if (err) {
                logger.error('There was an error reading ' + propertyFile);
                reject(err);
                return;
            }

            var parsed = propertiesParser.parse(data);
            var theme  = utility.validateThemeParameters(parsed, identification);
            fulfill(theme); 
        }); 
    });
}

function writeJsonFile(path, data) {
    return new Promise(function (fulfill, reject) {
        fs.writeFile(path, data, function (err, data) {
            if (err) {
                logger.error('There was an error writing ' + path);
                reject(err);
                return;
            }
            fulfill(); 
        });
    });
}

function generateLess(identification, themeObj) {
    return new Promise(function (fulfill, reject) {
        //get theme wrapper;
        var theme = themeWrapperLess.replace('{{identification}}', identification);
        //if it's the system theme, don't use wrapper;
        if (identification === "system") {
            theme = themeLess;
        }
        
        theme = '@import "variables";\n' + theme;
        
        fs.writeFile(path.join(tempFolder, 'theme.less'), theme, function (err, data) {
            if (err) {
                logger.error('There was an error writing theme.less for ' + identification);
                reject(err);
                return;
            }
            
            fs.writeFile(path.join(tempFolder, 'theme-content.less'), themeLess, function (err, data) {
                if (err) {
                    logger.error('There was an error writing theme-content.less for ' + identification);
                    reject(err);
                    return;
                }
                
                var variables = utility.setLessVariables(variablesLess, themeObj);
                fs.writeFile(path.join(tempFolder, 'variables.less'), variables, function (err, data) {
                    if (err) {
                        logger.error('There was an error writing variables.less for ' + identification);
                        reject(err);
                        return;
                    }
                    
                    fulfill(theme);
                });        
            });    
        }); 
    });
}

function processLess(content) {
    return less.render(content, {
        paths: [tempFolder],
        filename: 'theme.less',
        compress: true,
    });
}

function saveLess(path, output) {
    return new Promise(function (fulfill, reject) {
        fs.writeFile(path, output.css, function (err, data) {
            if (err) {
                logger.error('There was an error writing css for ' + identification);
                reject(err);
                return;
            }
            
            fulfill();
        }); 
    });
}

function processImageName(name) {
    var extension = path.extname(name);
    return name.replace(extension, '_' + utility.generateRandomName() + extension);
}

module.exports = function processTheme(propertyFile, cssFileName, settings) {
    tempFolder = path.join(settings.outputFolder, 'themes_temp'); 
    //Get theme's folder
    var dir = path.dirname(propertyFile);
    //Get theme's folder identification
    var folders = dir.split(path.sep);
    var identification = folders[folders.length - 1];
    logger.info('Building theme ' + identification);
    //Create output Directory
    var outputDir = path.join(settings.outputFolder, identification);
    mkdirp.sync(outputDir);
    var themeObj;
    
    //Generate Json File
    logger.keyValue('Reading properties for', identification);
    return generateJson(propertyFile, identification)
    //Then Generage Less Files
    .then(function (theme) {
        themeObj = theme;
        logger.keyValue('Generating Less files for', identification);
        mkdirp.sync(tempFolder);
        return generateLess(identification, themeObj);        
    })
    //Then build css from less
    .then(processLess)
    //Then save the css to the theme's folder
    .then(function (output) {
        return saveLess(path.join(outputDir, cssFileName), output);
    })
    //Then copy image files to the theme's folder.
    .then(function () {
        for (var j in configKeys.copyKeys) {
            var key = configKeys.copyKeys[j];
            var fileName = themeObj[key];
            var origin = path.join(dir, fileName);
            var newFileName = processImageName(fileName);
            var destination = path.join(outputDir, newFileName);
            themeObj[key] = newFileName;
            fs.createReadStream(origin).pipe(fs.createWriteStream(destination));
        }
        
        //Delete temporary folder after usage
        rimraf.sync(tempFolder);
        
        return Promise.resolve();   
    })
    //Then save json file.
    .then(function() {
        logger.keyValue('Writing json for', identification);
        return writeJsonFile(path.join(outputDir, 'theme.json'), JSON.stringify(themeObj, null, 4));
    })
    .then(function() {
        return Promise.resolve({
            identification: identification,
            data: themeObj
        }); 
    });   
};