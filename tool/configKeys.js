module.exports = { 
    keys: {
        "header-logo": "headerLogo",
        "footer-logo": "footerLogo",
        "email-logo": "emailLogo",
        "login-logo": "loginLogo",
        "favicon": "favicon",
        "header-color": "headerColor",
        "header-text-color": "headerTextColor",
        "hyperlink-active-color": "hyperlinkActiveColor",
        "primary-color": "primaryColor",
        "primary-button-color": "primaryButtonColor",
        "email-divider-color": "emailDividerColor",
        "selected-item-color": "selectedItemColor",
        "selected-item-text-color": "selectedItemTextColor"
    },
    copyKeys: [
        "headerLogo",
        "footerLogo",
        "emailLogo",
        "loginLogo",
        "favicon"
    ]
};