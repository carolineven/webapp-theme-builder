# WebApp Theme Builder
----------------------

This tool allows you to build themes for Safe Share for Web.

Check [safeshare-themes](https://bitbucket.org/covata/safeshare-themes/overview) for some example themes. 

Recommendations:
---------------
It is recommeded that:

1. This repository is forked so you can commit and maintain your own themes in a repository that you own.

Dependencies:
------------
This tool requires that the following dependencies are installed:

1. Node.js - https://nodejs.org/en/

Getting Started:
---------------

1. Install node.js.
2. Fork and Clone this respository.
3. Go to the folder where the repository was cloned.
4. Open your terminal if you're not already using it.
5. Run the command ```npm run initialize``` so the tool can install its dependencies and set itself up.
6. Build your themes (instructions for this are in the next section).
7. Compile your themes running ```buildthemes``` (this command is available for global usage in any folder).
8. Your themes will be in the ```output``` directory ready for deployment.
9. Put the ```output``` folder in the webapp root directory and rename it to ```themes```.

Make you theme:
--------------

In order to make a new theme follow the steps below:

1. Create a folder a ```sources``` folder somewhere on your harddrive.
    * Preferebly in a folder with version control. (Could be inside the folder where this tool was cloned).
2. Create a folder for your theme inside the ```sources``` folder.
    * The name of the theme folder can be either ```system``` for the system admin/fallback theme or the id of the organization for which the theme is being created.
3. Copy the ```theme.properties``` file inside ```exemples``` to your theme's folder and configure the variables with the colors and images for your theme.
4. Add all the images to your theme's folder.
    * email-logo.(png|jpg|gif|bmp) - logo that is going to be used in emails.
    * footer-logo.(png|jpg|gif|bmp) - small logo that is shown in the footer of the application.
    * header-logo.(png|jpg|gif|bmp) - the logo that is shown in the header of the application.
    * login-logo.(png|jpg|gif|bmp) - the logo that is shown in the login screen (only the system theme one is shown in the login screen).
    * favicon.ico - a 16x16 favicon for the organisation.
5. From outside the ```sources``` folder run the ```buildthemes``` command as explained in "Getting Started" section.

Other options:
--------------

The ```buildthemes``` command has some options.
For help on what you can do with the tool, type ```buildthemes --help``` on your terminal.

#### Summary:
-------------

1. ```-c, --config``` - allows you to load your own configuration file (based on the settings.json in the root of this repository). 
    * Example on mac or linux: ```buildthemes -c /User/someuser/themefolder/settings.json```.
    * Example on Windows: ```buildthemes -c c:\Users\someuser\themefolder\settings.json```.
    * You can use just settings.json if the file is in the folder you're currently at.
2. ```-o, --output``` - allows you to set an output directory without having to configure a settings file.
    * Example on mac or linux: ```buildthemes -o /User/someuser/themefolder/output```.
    * Example on Windows: ```buildthemes -o c:\Users\someuser\themefolder\output```.
    * You can use just the folder name (eg. 'output') if you want to output to the folder you're currently at.
3. ```-o, --output``` - allows you to set a different source directory without having to configure a settings file.
    * Example on mac or linux: ```buildthemes -s /User/someuser/themefolder/sources```.
    * Example on Windows: ```buildthemes -s c:\Users\someuser\themefolder\sources```.
    * You can use just the folder name (eg. 'sources') if you want to use a source folder in the folder you're currently at.

#### theme.properties properties

* ```header-logo``` - The logo that is shown in the header of the application.
* ```footer-logo``` - The logo that is shown in the footer of the application.
* ```email-logo``` - The logo that is shown in emails that are sent by the application.
* ```login-logo``` - The logo that is shown in the login screen and other related screens.
* ```favicon``` - The icon that is shown in the browser's tab. 
* ```header-color``` - The background color for the application header.
* ```header-text-color``` - The text color for the application header.
* ```hyperlink-active-color``` - Text color for the links in the header.
* ```primary-color``` - The primary (accent) color of the application.
* ```primary-button-color``` - The background color for primary buttons.
* ```email-divider-color``` - The color for the dividers in the emails that are sent by the application.
* ```selected-item-color``` - The background color of the selected row in the main file list.
* ```selected-item-text-color``` - The text color of the selected row in the main file list.